# CLEARSCORE Interview chalange 

This repository contains source code for my sollution to clear score interview test.
This Document will describe:

 * Implementation of the sollution
 * Getting started guide
 * Project Structure

## Implementation 

Sollution is based mostly on terraform code which sets-up basic networking, s3, security groups, autoscaling group(using pre bcked ami) and load balancer.
In order to meet objectives of the test, route53 and Cloud Front Distribution have been setup. Unfortuantely with some manual steps. 

## Prerequisites

 * Pre baked ami with apache and webpage and source code of a website
 * AWS profile configured 
 * S3 bucket to store terraform state

## Getting Started 

Checkout the repo and apply terraform as foolow:

 * vpc ./terraform/env/dev/vpc/website/
 * data ./terraform/env/dev/data/s3/website/
 * s3 ./terraform/env/dev/service/website/

In order to apply terrafomr run:
 ```
 terraform init
 terraform plan -out plan 
 terraform apply plan
 ```


