output "bucket_domain" {
  value = "${aws_s3_bucket.b.bucket_domain_name}"
}

output "bucket_id" {
  value = "${aws_s3_bucket.b.id}"
}
