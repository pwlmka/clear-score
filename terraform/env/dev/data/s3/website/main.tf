###################
# Set up provider #
###################
provider "aws" {
  region = "eu-west-1"
}

##################
# Create Backend #
##################
terraform { 
  backend "s3" {
    bucket = "terraform-pwlmka"
    key = "clear-score/env/dev/data/s3/website/state.tf"
    region = "eu-west-1"
  }
}

########################################
# Create S3 Bucket for Website content #
########################################

resource "aws_s3_bucket" "b" {
  bucket = "cs-pm.test.com"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}
