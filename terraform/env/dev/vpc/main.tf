###################
# Set up provider #
###################
provider "aws" {
  region = "eu-west-1"
}

##################
# Create Backend #
##################
terraform { 
  backend "s3" {
    bucket = "terraform-pwlmka"
    key = "clear-score/env/dev/vpc/state.tf"
    region = "eu-west-1"
  }
}
module "vpc" {
  source = "git::https://github.com/terraform-aws-modules/terraform-aws-vpc.git"

  name = "cs-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}
