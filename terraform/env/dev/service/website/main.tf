###################
# Set up provider #
###################
provider "aws" {
  region = "eu-west-1"
}

data "terraform_remote_state" "bucket" {
  backend = "s3"
  config {
    bucket = "terraform-pwlmka"
    key = "clear-score/env/dev/data/s3/website/state.tf"
    region = "eu-west-1"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3" 
  config {
    bucket = "terraform-pwlmka"
    key = "clear-score/env/dev/vpc/state.tf"
    region = "eu-west-1"
  }
}

##################
# Create Backend #
##################
terraform { 
  backend "s3" {
    bucket = "terraform-pwlmka"
    key = "clear-score/dev/service/website"
    region = "eu-west-1"
  }
}

##############
# Get AMI ID #
##############
data "aws_ami" "web_ami" {
  filter {
    name   = "name"
    values = ["${var.website_ami_name}"]
  }
}

###############
# Get Regions #
###############
data "aws_availability_zones" "all" {}

##############################
# Security group for website #
##############################
resource "aws_security_group" "instance" {
  name = "website-instance"
  vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

############################################
# Launch Configuraration for website hosts #
############################################
resource "aws_launch_configuration" "web_launch_config" {
  image_id = "${data.aws_ami.web_ami.id}"
  instance_type = "${var.instance_type}"
  security_groups = ["${aws_security_group.instance.id}"]
  key_name = "${var.key_name}"
  lifecycle {
    create_before_destroy = true
  }
}

####################### 
# Create Auto Scaling #
#######################
resource "aws_autoscaling_group" "web_auto_scale" {
  launch_configuration = "${aws_launch_configuration.web_launch_config.id}"
  availability_zones = ["${data.aws_availability_zones.all.names}"]
  min_size = 2
  desired_capacity = 4
  max_size = 10
  vpc_zone_identifier = ["${data.terraform_remote_state.vpc.public_subnet_ids}"]  
  target_group_arns = ["${aws_lb_target_group.lb_target_group.arn}"]
  health_check_type = "ELB"

  tag {
    key = "Name"
    value = "website-asg"
    propagate_at_launch = true
  }
  tag {
    key = "application"
    value = "django"
    propagate_at_launch = true 
  }
  depends_on = ["aws_lb_target_group.lb_target_group"]
}

#################################
# Create Security Group for lb #
#################################
resource "aws_security_group" "lb_sg" {
  name = "website-lb-sg"
  vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

######################
# Create Website lb #
######################
resource "aws_lb" "web_lb" {
  name = "website-lb"
  internal = false
  subnets = ["${data.terraform_remote_state.vpc.public_subnet_ids}"]  
  security_groups = ["${aws_security_group.lb_sg.id}"]
}

#######################
# Create lb Listener #
#######################
resource "aws_lb_listener" "lb_listener" {  
  load_balancer_arn = "${aws_lb.web_lb.arn}"  
  port              = "80"  
  protocol          = "HTTP"
  
  default_action {    
    target_group_arn = "${aws_lb_target_group.lb_target_group.arn}"
    type             = "forward"  
  }
}

###########################
# Create lb Target Group #
###########################
resource "aws_lb_target_group" "lb_target_group" {  
  name     = "website-tg"  
  port     = "80"  
  protocol = "HTTP"  
  vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"

  tags {    
    name = "website-tg"    
  }   

  health_check {    
    healthy_threshold   = 3    
    unhealthy_threshold = 10    
    timeout             = 5    
    interval            = 10    
    path                = "/"    
    port                = "80"  
  }
}

#################################
# Create Autoscaling Attachment #
#################################
resource "aws_autoscaling_attachment" "asg_attach" {
  alb_target_group_arn   = "${aws_lb_target_group.lb_target_group.arn}"
  autoscaling_group_name = "${aws_autoscaling_group.web_auto_scale.id}"
}

##################
# Create Route53 #
##################
# resource "aws_route53_zone" "site_zone" {
#   name = "clearscore.pm.com"
# }


# when cloud fron is fixed update record with cdn cname
# resource "aws_route53_record" "www" {
#   zone_id = "${aws_route53_zone.site_zone.zone_id}"
#   name    = "${aws_route53_zone.site_zone.name}"
#   type    = "A"
#   ttl     = "300"
#   records = ["${aws_lb.web_lb.dns_name}"]
# }


# ###################################
# # Create Cloud Front Distribution #
# ###################################

#  resource "aws_cloudfront_distribution" "website_cdn" {
#    enabled = true
#    http_version = "http2"
#    origin {
#      domain_name = "${aws_lb.web_lb.dns_name}"
#      origin_id = "ELB-${element(split(".", aws_lb.web_lb.dns_name),0)}"
#    }


#    default_cache_behavior {
#      target_origin_id = "S3-${data.terraform_remote_state.bucket.bucket_id}"
#      allowed_methods = ["GET", "HEAD"]
#      cached_methods = ["GET", "HEAD"]
#      viewer_protocol_policy = "allow-all"
#      min_ttl = 0
#      default_ttl = 3600
#      max_ttl = 86400
#      compress = true
#      forwarded_values {
#        query_string = false
#        cookies {
#          forward = "none"
#        }
#      }
#    }
#    viewer_certificate {
#      cloudfront_default_certificate = true
#    }
#    restrictions {
#      geo_restriction {
#        restriction_type = "none"
#      }
#    }
#  }
