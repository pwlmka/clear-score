variable "website_ami_name" {
  description = "AMI of pre-baked image" 
  default = "website-1"
}

variable "count" {
  description = "Number of instances to launch" 
  default = 1
}

variable "key_name" {
  description = "Key name used to ssh to the machine"
  default = "website"
}

variable "instance_type" {
  default = "t2.micro"
}
